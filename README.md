# 10自由度惯性导航模块

10自由度惯性导航模块

<img src="arduinoC/_images/featured.png" width="450" height="300" align=center>

# 用户库教程


教程链接：[Mind+官方文档](https://mindplus.dfrobot.com.cn/extensions-user).

# 示例程序

![](https://img.dfrobot.com.cn/wiki/none/ed023e52e24d2264ed6af92bb2c7030d)

![](https://img.dfrobot.com.cn/wiki/none/e329feb327b7b3e0891462f68035d9fe)



# 更新日志
V0.0.1 基础功能完成

V0.0.2 根据Mind+V1.6.3 RC1.1更新此库